import pytest
from gidgetlab import sansio
from galaxy_bot import playbook_mr
from .utils import FakeGitLab


@pytest.mark.asyncio
async def test_update_playbooks_dependency_called_on_role(mocker):
    gl = FakeGitLab()
    mock = mocker.patch("galaxy_bot.awx_update.tasks.update_playbooks_dependency")
    user_id = 42
    name = "ics-ans-role-foo"
    tag = "v1.0.0"
    data = {
        "user_id": user_id,
        "ref": f"refs/tags/{tag}",
        "after": "8b9f3881cae5387dc969e66a03515cfe099cf088",
        "project": {"id": 551},
        "repository": {"name": name},
        "message": tag,
    }
    event = sansio.Event(data, event="Tag Push Hook")
    await playbook_mr.router.dispatch(event, gl)
    mock.assert_awaited_with(gl, name, tag, user_id, False)


@pytest.mark.asyncio
async def test_update_playbooks_dependency_called_on_role_do_not_merge_on_success(
    mocker,
):
    gl = FakeGitLab()
    mock = mocker.patch("galaxy_bot.awx_update.tasks.update_playbooks_dependency")
    user_id = 42
    name = "ics-ans-role-foo"
    tag = "v1.0.0"
    data = {
        "user_id": user_id,
        "ref": f"refs/tags/{tag}",
        "after": "8b9f3881cae5387dc969e66a03515cfe099cf088",
        "project": {"id": 551},
        "repository": {"name": name},
        "message": "v1.0.0\n\nDo not merge",
    }
    event = sansio.Event(data, event="Tag Push Hook")
    await playbook_mr.router.dispatch(event, gl)
    mock.assert_awaited_with(gl, name, tag, user_id, False)


@pytest.mark.asyncio
async def test_update_playbooks_dependency_called_not_on_role(mocker):
    gl = FakeGitLab()
    mock = mocker.patch("galaxy_bot.awx_update.tasks.update_playbooks_dependency")
    user_id = 42
    name = "ics-ans-foo"
    tag = "v1.0.0"
    data = {
        "user_id": user_id,
        "ref": f"refs/tags/{tag}",
        "after": "8b9f3881cae5387dc969e66a03515cfe099cf088",
        "project": {"id": 551},
        "repository": {"name": name},
    }
    event = sansio.Event(data, event="Tag Push Hook")
    await playbook_mr.router.dispatch(event, gl)
    mock.assert_not_awaited()


@pytest.mark.asyncio
async def test_update_playbooks_dependency_called_on_delete_tag(mocker):
    gl = FakeGitLab()
    mock = mocker.patch("galaxy_bot.awx_update.tasks.update_playbooks_dependency")
    user_id = 42
    name = "ics-ans-role-foo"
    tag = "v1.0.0"
    data = {
        "user_id": user_id,
        "ref": f"refs/tags/{tag}",
        "after": "0000000000000000000000000000000000000000",
        "project": {"id": 551},
        "repository": {"name": name},
    }
    event = sansio.Event(data, event="Tag Push Hook")
    await playbook_mr.router.dispatch(event, gl)
    mock.assert_not_awaited()
