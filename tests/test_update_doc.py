from galaxy_bot.settings import ANSIBLE_GALAXY_GROUP
import pytest
from gidgetlab import sansio
from gidgetlab_cli.models import Project
from galaxy_bot import update_doc
from .utils import FakeGitLab


@pytest.mark.asyncio
async def test_update_repo_master_branch(mocker):
    gl = FakeGitLab()
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    name = "a-project"
    data = {
        "ref": "refs/heads/master",
        "project": {"id": 551, "name": name, "namespace": f"group/{name}"},
    }
    event = sansio.Event(data, event="Push Hook")
    await update_doc.router.dispatch(event, gl)
    project = Project(**data["project"])
    mock_update.assert_awaited_with(
        gl, action="update", project=project, commit_message=f"Update {name}"
    )


@pytest.mark.asyncio
async def test_update_repo_not_master_branch(mocker):
    gl = FakeGitLab()
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    name = "a-project"
    data = {
        "ref": "refs/heads/test-branch",
        "project": {"id": 551, "name": name, "namespace": f"group/{name}"},
    }
    event = sansio.Event(data, event="Push Hook")
    await update_doc.router.dispatch(event, gl)
    mock_update.assert_not_awaited()


@pytest.mark.asyncio
async def test_update_repo_on_tag(mocker):
    gl = FakeGitLab()
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    name = "a-project"
    data = {
        "ref": "refs/tags/v0.1.0",
        "project": {"id": 551, "name": name, "namespace": f"group/{name}"},
    }
    event = sansio.Event(data, event="Tag Push Hook")
    await update_doc.router.dispatch(event, gl)
    project = Project(**data["project"])
    mock_update.assert_awaited_with(
        gl, action="update", project=project, commit_message=f"Update {name} on tag"
    )


@pytest.mark.asyncio
async def test_delete_repo_on_destroy(mocker):
    """Check that the task is called when a project from ics-ansible-galaxy is destroyed"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    path = "my-project"
    data = {
        "event_name": "project_destroy",
        "name": "my-project",
        "path": path,
        "path_with_namespace": "ics-ansible-galaxy/my-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_doc.router.dispatch(event, gl)
    project = Project(name=data["name"], namespace=ANSIBLE_GALAXY_GROUP)
    mock_update.assert_awaited_with(
        gl, action="delete", project=project, commit_message=f"Remove {path}"
    )


@pytest.mark.asyncio
async def test_delete_repo_on_galaxy_repo_renamed(mocker):
    """Check that the task is called when a project from ics-ansible-galaxy is renamed"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    data = {
        "event_name": "project_rename",
        "name": "my-new-project",
        "path": "my-new-project",
        "path_with_namespace": "ics-ansible-galaxy/my-new-project",
        "old_path_with_namespace": "ics-ansible-galaxy/my-old-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_doc.router.dispatch(event, gl)
    project = Project(name="my-old-project", namespace=ANSIBLE_GALAXY_GROUP)
    mock_update.assert_awaited_with(
        gl, action="delete", project=project, commit_message="Remove my-old-project"
    )


@pytest.mark.asyncio
async def test_rename_event_with_subgroups(mocker):
    """Check that no exception is raised when subgroups are used"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    data = {
        "event_name": "project_rename",
        "created_at": "2019-01-23T16:43:58Z",
        "updated_at": "2019-02-18T09:35:29Z",
        "name": "ps4000ioc",
        "path": "ps4000ioc",
        "path_with_namespace": "beam-diagnostics/bde/epics/iocs/ps4000ioc",
        "project_id": 795,
        "owner_name": "iocs",
        "owner_email": "",
        "project_visibility": "internal",
        "old_path_with_namespace": "beam-diagnostics/bde/epics/iocs/picoscopeioc",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_doc.router.dispatch(event, gl)
    mock_update.assert_not_awaited()


@pytest.mark.asyncio
async def test_delete_repo_on_galaxy_repo_transfered(mocker):
    """Check that the task is called when a project from  ics-ansible-galaxy is transfered"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    path = "my-project"
    data = {
        "event_name": "project_transfer",
        "name": "my-project",
        "path": path,
        "path_with_namespace": "new-group/my-project",
        "old_path_with_namespace": "ics-ansible-galaxy/my-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_doc.router.dispatch(event, gl)
    project = Project(name="my-project", namespace=ANSIBLE_GALAXY_GROUP)
    mock_update.assert_awaited_with(
        gl, action="delete", project=project, commit_message=f"Remove {path}"
    )


@pytest.mark.asyncio
async def test_delete_repo_on_galaxy_repo_archived(mocker):
    """Check that the task is called when a project from ics-ansible-galaxy is archived"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    project_id = 42
    data = {
        "event_name": "project_update",
        "project_id": project_id,
        "name": "deprecated-project",
        "path_with_namespace": "ics-ansible-galaxy/deprecated-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = FakeGitLab(getitem={"path": "deprecated-project", "archived": True})
    await update_doc.router.dispatch(event, gl)
    assert gl.getitem_url == f"/projects/{project_id}"
    project = Project(name=data["name"], namespace=ANSIBLE_GALAXY_GROUP)
    mock_update.assert_awaited_with(
        gl, action="delete", project=project, commit_message="Remove deprecated-project"
    )


@pytest.mark.asyncio
async def test_delete_repo_not_called_on_galaxy_repo_update(mocker):
    """Check that the task is called when a project from ics-ansible-galaxy is just updated (not archived)"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    project_id = 43
    data = {
        "event_name": "project_update",
        "project_id": project_id,
        "path_with_namespace": "ics-ansible-galaxy/my-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = FakeGitLab(getitem={"path": "my-project", "archived": False})
    await update_doc.router.dispatch(event, gl)
    assert gl.getitem_url == f"/projects/{project_id}"
    mock_update.assert_not_awaited()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "event_name",
    (
        "project_create",
        "user_add_to_team",
        "user_remove_from_team",
        "user_create",
        "user_destroy",
        "user_failed_login",
        "user_rename",
        "key_create",
        "key_destroy",
        "group_create",
        "group_destroy",
        "foo",
    ),
)
async def test_delete_repo_not_called_on_other_events(mocker, event_name):
    """Check that the task is not called on other events"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    data = {"event_name": event_name}
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_doc.router.dispatch(event, gl)
    mock_update.assert_not_awaited()


@pytest.mark.asyncio
@pytest.mark.parametrize("event_name", ("project_destroy", "project_update"))
async def test_delete_repo_not_called_on_non_galaxy_group(mocker, event_name):
    """Check that the task is not called on project destroy or update from another group"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    path = "my-project"
    data = {
        "event_name": event_name,
        "name": "my-project",
        "path": path,
        "path_with_namespace": "my-group/my-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_doc.router.dispatch(event, gl)
    mock_update.assert_not_awaited()


@pytest.mark.asyncio
@pytest.mark.parametrize("event_name", ("project_transfer", "project_rename"))
async def test_delete_repo_not_called_on_non_old_galaxy_group(mocker, event_name):
    """Check that the task is not called on project transfer or rename from another group"""
    mock_update = mocker.patch("galaxy_bot.update_doc.tasks.update_galaxy_info")
    path = "my-project"
    data = {
        "event_name": event_name,
        "name": "my-project",
        "path": path,
        "path_with_namespace": "ics-ansible-galaxy/my-project",
        "old_path_with_namespace": "my-group/my-cool-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_doc.router.dispatch(event, gl)
    mock_update.assert_not_awaited()
