import pytest
import http
import httpx
import respx
import yaml
from .utils import FakeGitLab, sample
from galaxy_bot import util
from galaxy_bot.settings import GALAXY_SUMMARY
from gidgetlab_cli.models import Project, Tag
from gidgetlab import HTTPException
from httpx import Response


LINES = [
    "---",
    "- src: git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-repository.git",
    "  version: v0.1.0",
    "- src: git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-powerdns.git",
    "- src: git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-devenv-base.git",
    "  version: v1.0.1",
    "- name: squid",
    "  scm: git",
    "  src: git@gitlab.esss.lu.se:ics-ansible-galaxy/ics-ans-role-squid.git",
    "  version: v0.1.0",
]


@pytest.fixture
def requirements_yml(tmpdir):
    return "\n".join(LINES)


def test_name_from_src():
    src = "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-repository.git"
    assert util.name_from_src(src) == "ics-ans-role-repository"
    src = "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-repository"
    assert util.name_from_src(src) == "ics-ans-role-repository"
    src = "git@gitlab.esss.lu.se:ics-ansible-galaxy/ics-ans-role-squid.git"
    assert util.name_from_src(src) == "ics-ans-role-squid"


def check_update_requirements(req_yml, role, version, line_nb, insert=False):
    updated = util.update_requirements(req_yml, role, version)
    lines = LINES.copy()
    new_line = f"  version: {version}"
    if insert:
        lines.insert(line_nb, new_line)
    else:
        lines[line_nb] = new_line
    assert updated == "\n".join(lines) + "\n"


def test_update_requirements_basic_case(requirements_yml):
    check_update_requirements(requirements_yml, "ics-ans-role-devenv-base", "v2.0.0", 5)


def test_update_requirements_name_alias(requirements_yml):
    """Name alias different from the repo name in url"""
    check_update_requirements(requirements_yml, "ics-ans-role-squid", "v1.0.0", 9)


def test_update_requirements_no_version(requirements_yml):
    """No version specified in the initial requirements.yml"""
    check_update_requirements(
        requirements_yml, "ics-ans-role-powerdns", "v3.0.1", 4, insert=True
    )


@respx.mock
@pytest.mark.asyncio
async def test_playbooks_depending_on_role():
    all_playbooks = {
        "first-playbook": {
            "name": "first-playbook",
            "requirements": [
                {
                    "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/another-role.git",
                    "version": "v2.0.0",
                },
                {
                    "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/my-role.git",
                    "version": "v1.0.0",
                },
            ],
        },
        "second-playbook": {
            "name": "second-playbook",
            "requirements": [
                {
                    "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/another-role.git",
                    "version": "v1.0.0",
                }
            ],
        },
        "third-playbook": {
            "name": "third-playbook",
            "requirements": [
                {
                    "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/my-role.git",
                    "version": "v1.1.0",
                }
            ],
        },
    }

    summary = {"playbooks": all_playbooks, "roles": {}}
    respx.get(str(GALAXY_SUMMARY)).mock(
        return_value=Response(status_code=200, json=summary)
    )
    playbooks = await util.playbooks_depending_on_role("my-role")
    assert playbooks == [
        playbook
        for playbook in all_playbooks.values()
        if playbook["name"] in ("first-playbook", "third-playbook")
    ]


@respx.mock
@pytest.mark.asyncio
async def test_playbooks_depending_on_role_404():
    respx.get(str(GALAXY_SUMMARY)).mock(return_value=Response(status_code=404))
    with pytest.raises(httpx.HTTPError):
        await util.playbooks_depending_on_role("my-role")


@pytest.mark.parametrize(
    "filename, name, version, dependencies",
    [
        ("meta_no_dep.yml", "repository", "0.4.0", []),
        (
            "meta_dep_simple_list.yml",
            "mongodb",
            "",
            [{"role": "ics-ans-role-repository"}],
        ),
        (
            "meta_dep_role_list.yml",
            "traefik",
            "2.0",
            [{"role": "ics-ans-role-docker"}, {"role": "ics-ans-role-certificate"}],
        ),
    ],
)
@pytest.mark.asyncio
async def test_parse_ansible_role(mocker, filename, name, version, dependencies):
    mock_get_file = mocker.patch(
        "galaxy_bot.util.api.get_file_from_repository", return_value=sample(filename)
    )
    if version == "":
        tag = None
    else:
        tag = Tag(name=version, target="target")
    mock_get_latest_tag = mocker.patch(
        "galaxy_bot.util.api.get_latest_tag_on_branch", return_value=tag
    )
    # Get galaxy_info from file
    yaml_file = yaml.safe_load(sample(filename))
    galaxy_info = yaml_file["galaxy_info"]
    gl = FakeGitLab()
    project = Project(name=name, id=12, namespace="my-group")
    repo = await util.parse_ansible_role(gl, project)
    mock_get_file.assert_awaited_with(gl, project.id, "meta/main.yml", "master")
    mock_get_latest_tag.assert_awaited_with(gl, project.id, "master")
    assert repo == {
        "type": "role",
        "name": project.name,
        "tag": version,
        "galaxy_info": galaxy_info,
        "dependencies": dependencies,
    }


@pytest.mark.asyncio
async def test_parse_ansible_role_file_not_found(mocker):
    mock_get_file = mocker.patch(
        "galaxy_bot.util.api.get_file_from_repository",
        side_effect=HTTPException(status_code=http.HTTPStatus.NOT_FOUND),
    )
    mock_get_latest_tag = mocker.patch("galaxy_bot.util.api.get_latest_tag_on_branch")
    gl = FakeGitLab()
    project = Project(name="foo", id=12, namespace="my-group")
    repo = await util.parse_ansible_role(gl, project)
    mock_get_file.assert_awaited_with(gl, project.id, "meta/main.yml", "master")
    mock_get_latest_tag.assert_not_awaited()
    assert repo is None


@pytest.mark.asyncio
async def test_parse_ansible_role_exception_raised(mocker):
    mock_get_file = mocker.patch(
        "galaxy_bot.util.api.get_file_from_repository",
        side_effect=HTTPException(status_code=http.HTTPStatus.UNAUTHORIZED),
    )
    mock_get_latest_tag = mocker.patch("galaxy_bot.util.api.get_latest_tag_on_branch")
    gl = FakeGitLab()
    project = Project(name="foo", id=12, namespace="my-group")
    with pytest.raises(HTTPException):
        await util.parse_ansible_role(gl, project)
    mock_get_file.assert_awaited_with(gl, project.id, "meta/main.yml", "master")
    mock_get_latest_tag.assert_not_awaited()


@pytest.mark.parametrize(
    "filename, name, requirements",
    [
        (
            "requirements.yml",
            "project1",
            [
                {
                    "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-repository.git",
                    "version": "v0.11.0",
                    "name": "ics-ans-role-repository",
                },
                {
                    "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-traefik.git",
                    "version": "v0.8.1",
                    "name": "ics-ans-role-traefik",
                },
                {
                    "scm": "git",
                    "src": "git@gitlab.esss.lu.se:ics-ansible-galaxy/ics-ans-role-certificate.git",
                    "name": "ics-ans-role-certificate",
                    "version": "v0.3.0",
                },
            ],
        ),
        ("requirements_empty.yml", "project2", []),
    ],
)
@pytest.mark.asyncio
async def test_parse_ansible_playbook(mocker, filename, name, requirements):
    mock_get_file = mocker.patch(
        "galaxy_bot.util.api.get_file_from_repository", return_value=sample(filename)
    )
    # Get galaxy_info from file
    gl = FakeGitLab()
    project = Project(name=name, id=12, namespace="my-group")
    repo = await util.parse_ansible_playbook(gl, project)
    mock_get_file.assert_awaited_with(
        gl, project.id, "roles/requirements.yml", "master"
    )
    assert repo == {
        "type": "playbook",
        "name": project.name,
        "requirements": requirements,
    }


@pytest.mark.asyncio
async def test_parse_ansible_playbook_file_not_found(mocker):
    mock_get_file = mocker.patch(
        "galaxy_bot.util.api.get_file_from_repository",
        side_effect=HTTPException(status_code=http.HTTPStatus.NOT_FOUND),
    )
    gl = FakeGitLab()
    project = Project(name="foo", id=12, namespace="my-group")
    repo = await util.parse_ansible_playbook(gl, project)
    mock_get_file.assert_awaited_with(
        gl, project.id, "roles/requirements.yml", "master"
    )
    assert repo == {"type": "playbook", "name": project.name, "requirements": []}


@pytest.mark.asyncio
async def test_parse_ansible_playbook_exception_raised(mocker):
    mock_get_file = mocker.patch(
        "galaxy_bot.util.api.get_file_from_repository",
        side_effect=HTTPException(status_code=http.HTTPStatus.UNAUTHORIZED),
    )
    gl = FakeGitLab()
    project = Project(name="foo", id=12, namespace="my-group")
    with pytest.raises(HTTPException):
        await util.parse_ansible_playbook(gl, project)
    mock_get_file.assert_awaited_with(
        gl, project.id, "roles/requirements.yml", "master"
    )


@pytest.mark.asyncio
async def test_parse_ansible_repo_role(mocker):
    result = {"type": "role"}
    mock_parse_role = mocker.patch(
        "galaxy_bot.util.parse_ansible_role", return_value=result
    )
    mock_parse_playbook = mocker.patch("galaxy_bot.util.parse_ansible_playbook")
    gl = FakeGitLab()
    project = Project(name="foo", id=12, namespace="my-group")
    repo = await util.parse_ansible_repo(gl, project)
    mock_parse_role.assert_awaited_with(gl, project)
    mock_parse_playbook.assert_not_awaited()
    assert repo == result


@pytest.mark.asyncio
async def test_parse_ansible_repo_playbook(mocker):
    result = {"type": "playbook"}
    mock_parse_role = mocker.patch(
        "galaxy_bot.util.parse_ansible_role", return_value=None
    )
    mock_parse_playbook = mocker.patch(
        "galaxy_bot.util.parse_ansible_playbook", return_value=result
    )
    gl = FakeGitLab()
    project = Project(name="foo", id=12, namespace="my-group")
    repo = await util.parse_ansible_repo(gl, project)
    mock_parse_role.assert_awaited_with(gl, project)
    mock_parse_playbook.assert_awaited_with(gl, project)
    assert repo == result
