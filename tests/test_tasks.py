import json
from galaxy_bot.settings import ANSIBLE_GALAXY_PROJECT_ID
import pytest
import respx
from galaxy_bot import tasks
from gidgetlab_cli.models import Project
from .utils import FakeGitLab, sample, json_sample
from httpx import Response


@respx.mock
@pytest.mark.asyncio
async def test_create_awx_project_exists():
    name = "my-project"
    request = respx.get(
        f"https://my-host.example.org/api/v2/projects?name={name}"
    ).mock(
        return_value=Response(
            status_code=200, json={"count": 1, "results": [{"id": 1}]}
        )
    )
    await tasks.create_awx_project(name, "git")
    assert request.called
    req, _ = respx.calls[0]
    assert req.headers["Authorization"] == "Bearer secret"


@respx.mock
@pytest.mark.asyncio
async def test_create_awx_project():
    name = "my-project"
    base_url = "https://my-host.example.org/api/v2"
    request_projects = respx.get(base_url + f"/projects?name={name}").mock(
        return_value=Response(status_code=200, json={"count": 0, "results": []})
    )
    request_organizations = respx.get(base_url + "/organizations?name=Default").mock(
        return_value=Response(
            status_code=200, json={"count": 1, "results": [{"id": 1}]}
        )
    )
    request_credentials = respx.get(base_url + "/credentials?name=gitlab-bot-key").mock(
        return_value=Response(
            status_code=200, json={"count": 1, "results": [{"id": 23}]}
        )
    )
    return_value = {"name": name}
    request_post = respx.post(base_url + "/organizations/1/projects/").mock(
        return_value=Response(status_code=200, json=return_value)
    )
    result = await tasks.create_awx_project(name, "git", description="new project")
    assert request_projects.called
    assert request_organizations.called
    assert request_credentials.called
    assert request_post.called
    assert result == return_value


@pytest.mark.parametrize(
    "input_file, expected_file",
    [
        ("galaxy-info-org.json", "galaxy-info-org.json"),
        ("galaxy-info-org.json", "galaxy-info-org-not-sorted.json"),
        ("galaxy-info-org.json", "galaxy-info-all.json"),
    ],
)
@pytest.mark.asyncio
async def test_update_galaxy_info_all(mocker, input_file, expected_file):
    mock_get_file = mocker.patch(
        "galaxy_bot.tasks.api.get_file_from_repository", return_value=sample(input_file)
    )
    mock_generate = mocker.patch(
        "galaxy_bot.tasks.generate_galaxy_info", return_value=json_sample(expected_file)
    )
    mock_update_file = mocker.patch("galaxy_bot.tasks.api.update_file_in_repository")
    gl = FakeGitLab()
    await tasks.update_galaxy_info(gl, action="all")
    mock_get_file.assert_awaited_with(
        gl, project_id=ANSIBLE_GALAXY_PROJECT_ID, file_path="galaxy.json", ref="master"
    )
    mock_generate.assert_awaited_with(gl)
    if (
        input_file == expected_file
        or expected_file == "galaxy-info-org-not-sorted.json"
    ):
        mock_update_file.assert_not_awaited()
    else:
        # Sample file is prettified - remove "\n"
        content = json.dumps(json_sample(expected_file))
        mock_update_file.assert_awaited_with(
            gl,
            project_id=ANSIBLE_GALAXY_PROJECT_ID,
            file_path="galaxy.json",
            branch="master",
            start_branch="master",
            content=content,
            commit_message="Update galaxy.info",
        )


@pytest.mark.parametrize(
    "input_file, name, expected_file",
    [
        ("galaxy-info-org.json", "foo", "galaxy-info-org.json"),
        (
            "galaxy-info-org.json",
            "ics-ans-alarm-config-logger",
            "galaxy-info-no-config-logger.json",
        ),
        (
            "galaxy-info-org.json",
            "ics-ans-role-alarm-annunciator",
            "galaxy-info-no-role-alarm-annunciator.json",
        ),
    ],
)
@pytest.mark.asyncio
async def test_update_galaxy_info_delete(mocker, input_file, name, expected_file):
    mock_get_file = mocker.patch(
        "galaxy_bot.tasks.api.get_file_from_repository", return_value=sample(input_file)
    )
    mock_update_file = mocker.patch("galaxy_bot.tasks.api.update_file_in_repository")
    gl = FakeGitLab()
    project = Project(name=name, namespace="foo")
    message = f"Delete project {name}"
    await tasks.update_galaxy_info(
        gl, action="delete", project=project, commit_message=message
    )
    mock_get_file.assert_awaited_with(
        gl, project_id=ANSIBLE_GALAXY_PROJECT_ID, file_path="galaxy.json", ref="master"
    )
    if input_file == expected_file:
        mock_update_file.assert_not_awaited()
    else:
        # Sample file is prettified - remove "\n"
        content = json.dumps(json_sample(expected_file))
        mock_update_file.assert_awaited_with(
            gl,
            project_id=ANSIBLE_GALAXY_PROJECT_ID,
            file_path="galaxy.json",
            branch="master",
            start_branch="master",
            content=content,
            commit_message=message,
        )


@pytest.mark.parametrize(
    "input_file, name, repo, expected_file",
    [
        (
            "galaxy-info-org.json",
            "ics-ans-role-zookeeper",
            {
                "dependencies": [{"role": "ics-ans-role-docker"}],
                "galaxy_info": {"author": "Anders Harrisson"},
                "name": "ics-ans-role-zookeeper",
                "tag": "",
                "type": "role",
            },
            "galaxy-info-role-zookeeper.json",
        ),
        (
            "galaxy-info-org.json",
            "ics-ans-bitbucket-backup",
            {
                "name": "ics-ans-bitbucket-backup",
                "requirements": [
                    {
                        "name": "ics-ans-role-repository",
                        "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-repository.git",
                        "version": "v0.11.0",
                    },
                    {
                        "name": "ics-ans-role-conda",
                        "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-conda.git",
                        "version": "v1.1.0",
                    },
                ],
                "type": "playbook",
            },
            "galaxy-info-org.json",
        ),
        (
            "galaxy-info-org.json",
            "ics-ans-bitbucket-backup",
            {
                "name": "ics-ans-bitbucket-backup",
                "requirements": [
                    {
                        "name": "ics-ans-role-repository",
                        "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-repository.git",
                        "version": "v0.11.0",
                    },
                    {
                        "name": "ics-ans-role-conda",
                        "src": "git+https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-conda.git",
                        "version": "v1.2.0",
                    },
                ],
                "type": "playbook",
            },
            "galaxy-info-bitbucket-updated.json",
        ),
    ],
)
@pytest.mark.asyncio
async def test_update_galaxy_info_update(mocker, input_file, name, repo, expected_file):
    mock_get_file = mocker.patch(
        "galaxy_bot.tasks.api.get_file_from_repository", return_value=sample(input_file)
    )
    mock_parse_repo = mocker.patch(
        "galaxy_bot.util.parse_ansible_repo", return_value=repo
    )
    mock_update_file = mocker.patch("galaxy_bot.tasks.api.update_file_in_repository")
    gl = FakeGitLab()
    project = Project(name=name, namespace="foo")
    message = f"Update project {name}"
    await tasks.update_galaxy_info(
        gl, action="update", project=project, commit_message=message
    )
    mock_get_file.assert_awaited_with(
        gl, project_id=ANSIBLE_GALAXY_PROJECT_ID, file_path="galaxy.json", ref="master"
    )
    mock_parse_repo.assert_awaited_with(gl, project)
    if input_file == expected_file:
        mock_update_file.assert_not_awaited()
    else:
        # Sample file is prettified - remove "\n"
        content = json.dumps(json_sample(expected_file))
        mock_update_file.assert_awaited_with(
            gl,
            project_id=ANSIBLE_GALAXY_PROJECT_ID,
            file_path="galaxy.json",
            branch="master",
            start_branch="master",
            content=content,
            commit_message=message,
        )
