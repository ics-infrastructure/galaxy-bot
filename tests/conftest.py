from starlette.config import environ


environ["TOWER_HOST"] = "my-host.example.org"
environ["TOWER_OAUTH_TOKEN"] = "secret"
environ["GL_SECRET"] = "12345"
