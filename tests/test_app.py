import pytest
from galaxy_bot.app import app
from starlette.testclient import TestClient


@pytest.fixture()
def client():
    """Make a 'client' fixture available to test cases."""
    # Our fixture is created within a context manager. This ensures that
    # application startup and shutdown run for every test case.
    with TestClient(app) as test_client:
        yield test_client


def test_dummy_webhook(client):
    """Check that the bot can process a webhook request"""
    headers = {"x-gitlab-event": "Dummy Hook", "x-gitlab-token": "12345"}
    data = {"msg": "testing webhook request"}
    response = client.post("/", headers=headers, json=data)
    assert response.status_code == 200


def test_health(client):
    """The server should answer 'Bot OK' on /health endpoint"""
    response = client.get("/health")
    assert response.status_code == 200
    assert response.text == "Bot OK"
