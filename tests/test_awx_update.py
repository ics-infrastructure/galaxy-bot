import pytest
from gidgetlab import sansio
from galaxy_bot import awx_update


@pytest.mark.asyncio
async def test_create_create_awx_project_not_called_on_role(mocker):
    gl = None
    mock = mocker.patch("galaxy_bot.awx_update.tasks.create_awx_project")
    name = "ics-ans-role-foo"
    data = {"repository": {"name": name}}
    event = sansio.Event(data, event="Push Hook")
    await awx_update.router.dispatch(event, gl)
    mock.assert_not_awaited()


@pytest.mark.asyncio
async def test_create_create_awx_project_called_on_playbook(mocker):
    gl = None
    mock = mocker.patch("galaxy_bot.awx_update.tasks.create_awx_project")
    name = "ics-ans-foo"
    description = "Playbook to deploy foo"
    scm_url = "git@gitlab.example.com:test-group/foo.git"
    data = {
        "repository": {"name": name, "description": description, "git_ssh_url": scm_url}
    }
    event = sansio.Event(data, event="Push Hook")
    await awx_update.router.dispatch(event, gl)
    mock.assert_awaited_with(name, scm_url, description=description)
