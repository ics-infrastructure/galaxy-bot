from typer.testing import CliRunner
from galaxy_bot.command import cli

runner = CliRunner()


def test_cli_subcommands():
    result = runner.invoke(cli, ["--help"])
    assert result.exit_code == 0
    for command in ("update-galaxy-info", "update-playbooks-dependency"):
        assert command in result.output


# async def test_dummy_webhook(aiohttp_client):
#     """Check that the bot can process a webhook request"""
#     client = await aiohttp_client(bot.app)
#     headers = {"x-gitlab-event": "Dummy Hook"}
#     data = {"msg": "testing webhook request"}
#     response = await client.post("/", headers=headers, json=data)
#     assert response.status == 200
