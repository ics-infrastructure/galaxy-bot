import asyncio
import json
import logging
import typer
import httpx
from importlib.metadata import version, PackageNotFoundError  # type: ignore
from typing import Optional
from gidgetlab.httpx import GitLabAPI
from . import tasks


try:
    __version__ = version("galaxy-bot")
except PackageNotFoundError:
    __version__ = "unknown"

cli = typer.Typer()
state = {"gl": None}


def version_callback(value: bool):
    if value:
        typer.echo(f"galaxy-bot version: {__version__}")
        raise typer.Exit()


@cli.callback()
def main(
    version: Optional[bool] = typer.Option(
        None,
        "--version",
        callback=version_callback,
        is_eager=True,
        help="Show the current version and exit.",
    ),
    url: str = typer.Option(
        "https://gitlab.esss.lu.se", envvar="GL_URL", help="GitLab URL"
    ),
    access_token: str = typer.Option(
        "", envvar="GL_ACCESS_TOKEN", help="GitLab access token"
    ),
):
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )
    client = httpx.AsyncClient()
    state["gl"] = GitLabAPI(client, "galaxy-bot", url=url, access_token=access_token)


@cli.command()
def update_galaxy_info():
    """Update the galaxy.info file in the ansible-galaxy project"""
    asyncio.run(tasks.update_galaxy_info(state["gl"], action="all"))


@cli.command()
def generate_galaxy_info(filename: str = "galaxy-info.json", pretty: bool = False):
    """Generate the galaxy-info.json file"""
    info = asyncio.run(tasks.generate_galaxy_info(state["gl"]))
    if pretty:
        content = json.dumps(info, sort_keys=True, indent=4)
    else:
        content = json.dumps(info)
    with open(filename, "w") as f:
        f.write(content)


@cli.command()
def update_playbooks_dependency(
    role: str = typer.Option(..., help="Name of the role to update"),
    version: str = typer.Option(..., help="Version of the role to use"),
    merge_on_success: bool = typer.Option(
        True, help="Accept the merge request on success"
    ),
):
    """Update the role version in all playbooks requirements.yml"""
    asyncio.run(
        tasks.update_playbooks_dependency(
            state["gl"], role, version, None, merge_on_success
        )
    )


if __name__ == "__main__":
    cli()
