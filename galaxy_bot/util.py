import asyncio
import http
import logging
from gidgetlab.abc import GitLabAPI
from gidgetlab import HTTPException
from gidgetlab_cli.models import Project
import httpx
import yaml
from typing import Dict, Optional
from gidgetlab import GitLabException
from gidgetlab_cli import api
from .settings import GALAXY_SUMMARY


logger = logging.getLogger(__name__)


def name_from_src(src):
    """Return the repo name based on the source url"""
    name = src.split("/")[-1]
    if name.endswith(".git"):
        return name[:-4]
    else:
        return name


async def playbooks_depending_on_role(role_name):
    """Return the list of playbooks that depend on the role"""
    async with httpx.AsyncClient() as client:
        r = await client.get(str(GALAXY_SUMMARY))
    r.raise_for_status()
    info = r.json()
    return [
        playbook
        for playbook in info["playbooks"].values()
        if role_name
        in [
            name_from_src(requirement["src"])
            for requirement in playbook["requirements"]
        ]
    ]


def update_requirements(content, role_name, tag):
    """Return an updated requirements.yml file content with role set to version"""
    requirements = yaml.safe_load(content)
    for requirement in requirements:
        if role_name == name_from_src(requirement["src"]):
            requirement["version"] = tag
            break
    return yaml.safe_dump(requirements, default_flow_style=False, explicit_start=True)


async def create_playbook_mr(gl, project_id, role_name, tag, user_id, merge_on_success):
    """Create a merge request to update the role version in the playbook"""
    file_path = "roles/requirements.yml"
    branch = f"{role_name}_{tag}"
    branch_default = await api.get_default_branch(gl, project_id)
    content = await api.get_file_from_repository(
        gl, project_id, file_path, branch_default
    )
    updated_content = update_requirements(content, role_name, tag)
    if updated_content == content:
        logger.info(f"Role {role_name} already set to {tag} for project {project_id}")
        return
    logger.info(f"Updating role {role_name} to {tag} for project {project_id}")
    try:
        await api.update_file_via_merge_request(
            gl,
            project_id,
            file_path,
            updated_content,
            branch,
            branch_default,
            f"Update dependency {role_name} to {tag}",
            assignee_id=user_id,
            merge_on_success=merge_on_success,
            unsubscribe=True,
            wait_on_merge=False,
        )
    except GitLabException:
        logging.error(
            f"An exception occured while updating {project_id}", exc_info=True
        )


async def safe_create_playbook_mr(
    semaphore, gl, project_id, role_name, tag, user_id, merge_on_success
):
    """Use a semaphore to limit the number of simultaneous merge requests created"""
    async with semaphore:
        await create_playbook_mr(
            gl, project_id, role_name, tag, user_id, merge_on_success
        )


async def parse_ansible_role(gl: GitLabAPI, project: Project) -> Optional[Dict]:
    """Get the Ansible info about the given role project

    Return None if no meta/main.yml is found
    """
    try:
        meta_file = await api.get_file_from_repository(
            gl, project.id, "meta/main.yml", "master"
        )
    except HTTPException as e:
        if e.status_code == http.HTTPStatus.NOT_FOUND:
            # This isn't an Ansible role
            return None
        else:
            raise
    info = yaml.safe_load(meta_file)
    # Make sure dependencies is a list of dict
    # and not list of roles (as string)
    dependencies = []
    for dependency in info.get("dependencies", []):
        if isinstance(dependency, dict):
            dependencies.append(dependency)
        else:
            dependencies.append({"role": dependency})
    info["dependencies"] = dependencies
    tag = await api.get_latest_tag_on_branch(gl, project.id, "master")
    if tag is None:
        logger.warning(f"WARNING! No tag found in {project.name}")
        info["tag"] = ""
    else:
        info["tag"] = tag.name
    info["type"] = "role"
    info["name"] = project.name
    return info


async def parse_ansible_playbook(gl: GitLabAPI, project: Project) -> Dict:
    """Get the Ansible info about the given playbook project"""
    try:
        requirements_file = await api.get_file_from_repository(
            gl, project.id, "roles/requirements.yml", "master"
        )
    except HTTPException as e:
        if e.status_code == http.HTTPStatus.NOT_FOUND:
            # Assume this is a playbook without dependency
            requirements = []
        else:
            raise
    else:
        requirements = yaml.safe_load(requirements_file) or []
    for requirement in requirements:
        if "name" not in requirement:
            requirement["name"] = requirement["src"].split("/")[-1].replace(".git", "")
    info = {"type": "playbook", "name": project.name, "requirements": requirements}
    return info


async def parse_ansible_repo(gl: GitLabAPI, project: Project) -> Dict:
    """Get Ansible information about the project"""
    logger.info(f"Parse Ansible repository {project.name}")
    # Assume it's a role
    info = await parse_ansible_role(gl, project)
    if info is None:
        # Assume it's a playbook
        info = await parse_ansible_playbook(gl, project)
    return info


async def safe_parse_ansible_repo(
    semaphore: asyncio.Semaphore, gl: GitLabAPI, project: Project
):
    """Use a semaphore to limit the number of simultaneous requests to parse Ansible repo"""
    async with semaphore:
        repo = await parse_ansible_repo(gl, project)
    return repo


async def update_role_or_playbook(
    gl: GitLabAPI, project: Project, galaxy_info: Dict
) -> None:
    """Update the galaxy_info dict with information from project"""
    repo = await parse_ansible_repo(gl, project)
    if repo["type"] == "role":
        galaxy_info["roles"][project.name] = repo
    elif repo["type"] == "playbook":
        galaxy_info["playbooks"][project.name] = repo
