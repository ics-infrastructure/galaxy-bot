import cachetools
import httpx
import gidgetlab
import gidgetlab.abc
import gidgetlab.routing
from typing import Any, Optional, Sequence, Union
from starlette.datastructures import Secret
from starlette.applications import Starlette
from starlette.responses import Response, PlainTextResponse
from starlette.requests import Request
from starlette.routing import Route
from starlette.background import BackgroundTask
from starlette.exceptions import HTTPException
from gidgetlab import sansio
from gidgetlab import httpx as gl_httpx


class GitLabBot(Starlette):
    def __init__(
        self,
        requester: str,
        *,
        secret: Union[str, Secret] = None,
        access_token: Union[str, Secret] = None,
        cache: Optional[gidgetlab.abc.CACHE_TYPE] = None,
        wait_consistency: Optional[bool] = True,
        routers: Sequence[gidgetlab.routing.Router] = None,
        debug: bool = False,
        **kwargs: Any,
    ) -> None:
        self.requester = requester
        self.secret = secret
        self.access_token = access_token
        self.cache = cache or cachetools.LRUCache(maxsize=500)
        self.wait_consistency = wait_consistency
        # Additional keyword arguments to pass to GitLabAPI (url and api_version)
        self.kwargs = kwargs
        if routers is not None:
            self._webhook_router = gidgetlab.routing.Router(*routers)
        routes = [
            Route("/", self.webhook_handler, methods=["POST"]),
            Route("/health", self.health_handler),
        ]
        super().__init__(
            debug=debug,
            routes=routes,
            on_startup=[self.create_gl],
            on_shutdown=[self.close_gl_client],
        )

    @property
    def webhook_router(self) -> gidgetlab.routing.Router:
        """The bot :class:`gidgetlab.routing.Router` instance that routes webhooks events callbacks"""
        if not hasattr(self, "_webhook_router"):
            self._webhook_router = gidgetlab.routing.Router()
        return self._webhook_router

    async def create_gl(self) -> None:
        """Startup handler that creates the GitLabAPI instance"""
        client = httpx.AsyncClient()
        self.state.gl = gl_httpx.GitLabAPI(
            client,
            self.requester,
            cache=self.cache,
            access_token=str(self.access_token),
            **self.kwargs,
        )

    async def close_gl_client(self) -> None:
        """Shutdown handler that closes the httpx client"""
        await self.state.gl._client.aclose()

    async def health_handler(self, request: Request) -> Response:
        """Handler to check the health of the bot

        Return 'Bot OK'
        """
        return PlainTextResponse("Bot OK")

    async def webhook_handler(self, request: Request) -> Response:
        """Handler that processes GitLab webhook requests"""
        body = await request.body()
        try:
            event = sansio.Event.from_http(request.headers, body, secret=self.secret)
        except gidgetlab.HTTPException as e:
            raise HTTPException(status_code=e.status_code, detail=str(e))
        except gidgetlab.GitLabException as e:
            raise HTTPException(status_code=500, detail=str(e))
        # Call the appropriate callback(s) for the event in a background task
        task = BackgroundTask(self.webhook_router.dispatch, event, request.app.state.gl)
        return Response(status_code=200, background=task)
