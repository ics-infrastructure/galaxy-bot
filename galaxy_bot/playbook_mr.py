import gidgetlab.routing
from . import tasks

router = gidgetlab.routing.Router()


@router.register("Tag Push Hook")
async def create_playbooks_mr(event, gl, *args, **kwargs):
    """Create a merge request for all playbooks depending on a role"""
    if event.data["after"] == "0000000000000000000000000000000000000000":
        # Tag deleted - nothing to do
        return
    name = event.data["repository"]["name"]
    # Only trigger the task when pushing a tag on a role
    if name.startswith("ics-ans-role"):
        merge_on_success = False
        tag = event.data["ref"].replace("refs/tags/", "")
        await tasks.update_playbooks_dependency(
            gl, name, tag, event.data["user_id"], merge_on_success
        )
