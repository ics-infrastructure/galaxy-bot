import logging
import sentry_sdk
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from . import update_doc, playbook_mr, awx_update
from .bot import GitLabBot
from . import settings


uvicorn_logger = logging.getLogger("uvicorn.error")
logger = logging.getLogger("galaxy_bot")
logger.setLevel(uvicorn_logger.level)


app = GitLabBot(
    "galaxy-bot",
    secret=settings.GL_SECRET,
    access_token=settings.GL_ACCESS_TOKEN,
    routers=[update_doc.router, playbook_mr.router, awx_update.router],
    url=str(settings.GL_URL),
)
if settings.SENTRY_DSN:
    sentry_sdk.init(dsn=settings.SENTRY_DSN)
    app = SentryAsgiMiddleware(app)
