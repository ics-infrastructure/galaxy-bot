import asyncio
import copy
import json
import logging
import httpx
from typing import Dict, Optional
from gidgetlab.abc import GitLabAPI
from gidgetlab_cli.models import Project
from gidgetlab_cli import api
from . import util
from .settings import (
    SIMULTANEOUS_MR,
    TOWER_HOST,
    TOWER_OAUTH_TOKEN,
    ANSIBLE_GALAXY_PROJECT_ID,
    ANSIBLE_GALAXY_GROUP,
    SIMULTANEOUS_PARSE_ANSIBLE_REPO,
    SKIP_MERGE_ON_SUCCESS_PLAYBOOKS,
)

logger = logging.getLogger(__name__)


async def update_playbooks_dependency(gl, role_name, tag, user_id, merge_on_success):
    """Update all playbooks that have the role as dependency"""
    logger.info(f"Update playbooks dependency {role_name} to {tag}")
    playbooks = [
        playbook["name"]
        for playbook in await util.playbooks_depending_on_role(role_name)
    ]
    # Use a semaphore to limit the number of concurrent requests made to the GitLab server
    semaphore = asyncio.Semaphore(SIMULTANEOUS_MR)
    merge_requests = [
        util.safe_create_playbook_mr(
            semaphore,
            gl,
            f"ics-ansible-galaxy%2F{playbook}",
            role_name,
            tag,
            user_id,
            merge_on_success
            if playbook not in list(SKIP_MERGE_ON_SUCCESS_PLAYBOOKS)
            else False,
        )
        for playbook in playbooks
    ]
    await asyncio.gather(*merge_requests)


async def create_awx_project(
    name: str,
    scm_url: str,
    *,
    description: str = "",
    organization: str = "Default",
    scm_credential: str = "gitlab-bot-key",
    scm_clean: bool = True,
    scm_delete_on_update: bool = True,
    scm_update_on_launch: bool = True,
) -> Dict:
    """Create a new project in AWX"""
    headers = {"Authorization": f"Bearer {TOWER_OAUTH_TOKEN}"}
    async with httpx.AsyncClient(
        base_url=f"https://{TOWER_HOST}/api/v2", headers=headers, verify=False
    ) as client:
        r = await client.get("/projects", params={"name": name})
        if r.json()["count"] != 0:
            logger.info(f"AWX project {name} already exists")
            return
        # Retrieve the organization and credential id
        r = await client.get("/organizations", params={"name": organization})
        org_id = r.json()["results"][0]["id"]
        r = await client.get("/credentials", params={"name": scm_credential})
        cred_id = r.json()["results"][0]["id"]
        logger.info(f"Create AWX project {name}...")
        payload = {
            "name": name,
            "description": description,
            "scm_type": "git",
            "scm_url": scm_url,
            "credential": cred_id,
            "scm_clean": scm_clean,
            "scm_delete_on_update": scm_delete_on_update,
            "scm_update_on_launch": scm_update_on_launch,
        }
        r = await client.post(f"/organizations/{org_id}/projects/", json=payload)
        return r.json()


async def update_galaxy_info(
    gl: GitLabAPI,
    action: str = "all",
    project: Optional[Project] = None,
    commit_message: str = "Update galaxy.info",
) -> None:
    """Compute or update the galaxy.info file if changed in the ansible galaxy repo"""
    galaxy_json = await api.get_file_from_repository(
        gl, project_id=ANSIBLE_GALAXY_PROJECT_ID, file_path="galaxy.json", ref="master"
    )
    current_info = json.loads(galaxy_json)
    info = copy.deepcopy(current_info)
    if action == "all":
        info = await generate_galaxy_info(gl)
    if action == "delete":
        info["roles"].pop(project.name, None)
        info["playbooks"].pop(project.name, None)
    elif action == "update":
        await util.update_role_or_playbook(gl, project, info)
    if info == current_info:
        logger.info("No change in galaxy.info")
        return
    await api.update_file_in_repository(
        gl,
        project_id=ANSIBLE_GALAXY_PROJECT_ID,
        file_path="galaxy.json",
        branch="master",
        start_branch="master",
        content=json.dumps(info),
        commit_message=commit_message,
    )


async def generate_galaxy_info(gl: GitLabAPI) -> Dict:
    info = {"roles": {}, "playbooks": {}}
    logger.debug(f"Looking for all projects in {ANSIBLE_GALAXY_GROUP} group")
    # Filter empty projects (default_branch is None)
    projects = [
        project
        async for project in api.get_all_projects(gl, ANSIBLE_GALAXY_GROUP)
        if project.default_branch is not None
    ]
    logger.debug(f"Found {len(projects)} projects")
    semaphore = asyncio.Semaphore(SIMULTANEOUS_PARSE_ANSIBLE_REPO)
    all_parse_ansible_repos = [
        util.safe_parse_ansible_repo(semaphore, gl, project) for project in projects
    ]
    ansible_repos = await asyncio.gather(*all_parse_ansible_repos)
    for repo in ansible_repos:
        if repo["type"] == "role":
            info["roles"][repo["name"]] = repo
        elif repo["type"] == "playbook":
            info["playbooks"][repo["name"]] = repo
    return info
