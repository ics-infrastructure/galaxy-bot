import gidgetlab.routing
from gidgetlab_cli.models import Project
from .settings import ANSIBLE_GALAXY_GROUP
from . import tasks

router = gidgetlab.routing.Router()


@router.register("Push Hook")
async def update_repo_on_push(event, gl, *args, **kwargs):
    """Update the ansible-galaxy repo with the role or playbook that was pushed"""
    branch = event.data["ref"].replace("refs/heads/", "")
    if branch not in ("master", "main"):
        # Only look at the master or main branch
        return None
    project = Project(**event.data["project"])
    await tasks.update_galaxy_info(
        gl, action="update", project=project, commit_message=f"Update {project.name}"
    )


@router.register("Tag Push Hook")
async def update_repo_on_tag(event, gl, *args, **kwargs):
    """Update the ansible-galaxy repo with the role that was tagged"""
    project = Project(**event.data["project"])
    await tasks.update_galaxy_info(
        gl,
        action="update",
        project=project,
        commit_message=f"Update {project.name} on tag",
    )


@router.register("System Hook")
async def delete_repo(event, gl, *args, **kwargs):
    """Remove the playbook or role that was destroyed, archived or renamed/transfered"""
    project = None
    if event.data["event_name"] in ("project_transfer", "project_rename"):
        # Remove the old name (the new name will be added when pushing to it)
        # Note that if subgroups are used, old_path can include "/"
        old_namespace, *_, old_name = event.data["old_path_with_namespace"].split("/")
        if old_namespace != ANSIBLE_GALAXY_GROUP:
            return
        project = Project(name=old_name, namespace=ANSIBLE_GALAXY_GROUP)
    elif not event.data.get("path_with_namespace", "").startswith(
        ANSIBLE_GALAXY_GROUP + "/"
    ):
        return
    elif event.data["event_name"] == "project_destroy":
        project = Project(name=event.data["name"], namespace=ANSIBLE_GALAXY_GROUP)
    elif event.data["event_name"] == "project_update":
        # check if the project was archived
        data = await gl.getitem(f"/projects/{event.data['project_id']}")
        if data["archived"]:
            project = Project(name=event.data["name"], namespace=ANSIBLE_GALAXY_GROUP)
    if project is None:
        return
    await tasks.update_galaxy_info(
        gl, action="delete", project=project, commit_message=f"Remove {project.name}"
    )
