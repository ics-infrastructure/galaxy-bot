from starlette.config import Config
from starlette.datastructures import Secret, URL, CommaSeparatedStrings

config = Config(".env")

GL_URL = config("GL_URL", cast=URL, default="https://gitlab.esss.lu.se")
# The personal access token to use the GitLab API
GL_ACCESS_TOKEN = config("GL_ACCESS_TOKEN", cast=Secret, default="secret-token")
# The secret used when creating the webhook
# As the default is None, we can't cast to Secret
GL_SECRET = config("GL_SECRET", cast=str, default=None)

GALAXY_SUMMARY = config(
    "GALAXY_SUMMARY",
    cast=URL,
    default="http://ics-infrastructure.pages.esss.lu.se/ansible-galaxy/api/summary",
)
ANSIBLE_GALAXY_PROJECT_ID = config(
    "ANSIBLE_GALAXY_PROJECT_ID", cast=str, default="ics-infrastructure%2Fansible-galaxy"
)
ANSIBLE_GALAXY_GROUP = config(
    "ANSIBLE_GALAXY_GROUP", cast=str, default="ics-ansible-galaxy"
)

# Sentry Data Source Name
# Leave it empty to disable it
SENTRY_DSN = config("SENTRY_DSN", cast=str, default="")

# AWX API parameters
TOWER_HOST = config("TOWER_HOST", cast=str, default="localhost")
TOWER_OAUTH_TOKEN = config("TOWER_OAUTH_TOKEN", cast=Secret, default="")

# Max number of simultaneous playbook merge requests
# Avoid overloading GitLab with too many concurrent requests
# Looks like creating new branches with a commit in parallel is what GitLab doesn't like...
# Even 4 was too much
SIMULTANEOUS_MR = config("SIMULTANEOUS_MR", cast=int, default=2)
# Max number of simultaneous requests to parse Ansible repo
# No post - only get so high number shall be ok
SIMULTANEOUS_PARSE_ANSIBLE_REPO = config(
    "SIMULTANEOUS_PARSE_ANSIBLE_REPO", cast=int, default=10
)
SKIP_MERGE_ON_SUCCESS_PLAYBOOKS = config(
    "SKIP_MERGE_ON_SUCCESS_PLAYBOOKS", cast=CommaSeparatedStrings, default=[]
)
