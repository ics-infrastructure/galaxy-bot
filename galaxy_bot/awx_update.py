import gidgetlab.routing
from typing import Any
from gidgetlab.httpx import GitLabAPI
from gidgetlab.sansio import Event
from . import tasks

router = gidgetlab.routing.Router()


@router.register("Push Hook")
async def create_awx_project(
    event: Event, gl: GitLabAPI, *args: Any, **kwargs: Any
) -> None:
    """Create a project in AWX for all playbooks pushed to ics-ansible-galaxy group"""
    # We trigger the task on "Push" and not "System Hook" (repository creation) because
    # AWX tries to clone the repository when creating a project and that fails when the
    # repo is empty. It also makes sure that if a project wasn't created for an old repo,
    # it will.
    name = event.data["repository"]["name"]
    # Only trigger the task when pushing to a playbook
    if not name.startswith("ics-ans-role"):
        scm_url = event.data["repository"]["git_ssh_url"]
        description = event.data["repository"]["description"]
        await tasks.create_awx_project(name, scm_url, description=description)
