# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

requirements = [
    "cachetools",
    "gidgetlab[httpx]",
    "gidgetlab-cli>=0.8.3",
    "gunicorn",
    "sentry-sdk",
    "starlette",
    "uvicorn",
    "PyYAML",
]

test_requirements = [
    "pytest>=3.0.0",
    "pytest-cov",
    "pytest-asyncio",
    "pytest-mock",
    "respx",
]

setup(
    name="galaxy-bot",
    author="Benjamin Bertrand",
    author_email="benjamin.bertrand@esss.se",
    description="GitLab bot to manages ics-ansible-galaxy repositories",
    url="https://gitlab.esss.lu.se/ics-infrastructure/galaxy-bot",
    license="MIT license",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    install_requires=requirements,
    tests_require=test_requirements,
    packages=find_packages(exclude=["tests", "tests.*"]),
    include_package_data=True,
    keywords="gitlab",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
    ],
    python_requires=">=3.8",
    entry_points={"console_scripts": ["galaxy-bot=galaxy_bot.command:cli"]},
    extras_require={"tests": test_requirements},
)
