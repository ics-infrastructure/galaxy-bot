# galaxy-bot

This GitLab bot responds to webhooks from repositories part of the [ics-ansible-galaxy] group.

It is used to:

- update (on push events) the [ansible-galaxy] repository that contains the galaxy.info file with information about all playbooks and roles.
  This repo is used to build the [ICS Ansible Galaxy website].
- delete repository (on project destroy, archive, rename or transfer events) from the [ansible-galaxy] galaxy.info file.
- create merge requests (on tag push events) for all playbooks that depend on a role when this role is tagged.
  MR are automatically accepted when the pipeline succeeds.
- create an AWX project when pushing a new playbook to the [ics-ansible-galaxy] group.
- Update the galaxy.info file based on the [ics-ansible-galaxy] group (manual command to run periodically).

If the merge requests should not be accepted when the pipeline succeeds, the tag commit message should include the words "do not merge"
(case-insensitive).

## Configuration

1. Create two webhooks in GitLab. Both should use the same secret token.

   - a webhook on the [ics-ansible-galaxy] group for **Push** And **Tag push** events.
   - a system hook to be triggered on **Repository update events** (this is to delete submodules when a project
     of the ics-ansible-galaxy group is destroyed).

2. Add the bot user to the [ansible-galaxy] repo and [ics-ansible-galaxy] group as maintainer.

3. Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) to allow the bot user
   to access GitLab API.

4. Create a token to access tower by running:

   ```bash
   $ tower-cli config host tower.example.com
   $ tower-cli login username
   Password:
   ```

   The last command will save a token to `~/.tower-cli.cfg` that you should save.

6. The following environment variables shall be passed to the bot:

   - GL_SECRET: the token secret used when creating the webhooks.
   - GL_ACCESS_TOKEN: the personal access token to allow operations via the API.
   - ANSIBLE_GALAXY_PROJECT_ID: id of the project with the galaxy.info file [ansible-galaxy]
   - ANSIBLE_GALAXY_GROUP: name of group with all Ansible roles and playbooks [ics-ansible-galaxy]
   - TOWER_HOST: the tower host
   - TOWER_OAUTH_TOKEN: the oauth token to access tower API via tower-cli

   This is done by the [ics-ans-role-galaxy-bot](https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-galaxy-bot) Ansible role.

## License

MIT

[ansible-galaxy]: https://gitlab.esss.lu.se/ics-infrastructure/ansible-galaxy
[ics-ansible-galaxy]: https://gitlab.esss.lu.se/ics-ansible-galaxy
[ICS Ansible Galaxy website]: http://ics-infrastructure.pages.esss.lu.se/ansible-galaxy/
