FROM python:3.8-slim

# Installed required packages
RUN apt-get update \
  && apt-get install -yq --no-install-recommends \
    git \
    openssh-client \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Add csi user and gitlab.esss.lu.se key to know_hosts
# so that we can clone repos without having to confirm
# the authenticity of the host
RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -m -r -g csi -u 1000 csi \
  && mkdir /home/csi/.ssh \
  && echo 'gitlab.esss.lu.se ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIZogiM0g7EiyXGkADAfVqbf36zYb/EQUiirKALEE/Y7UjqhuG1pnkz0EvXUc3MMzPyQ8EEptjz3h+ZmNPV46XM=' > /home/csi/.ssh/known_hosts \
  && chown -R csi:csi /home/csi/.ssh \
  && chmod 0700 /home/csi/.ssh \
  && mkdir /home/csi/data \
  && chown csi:csi /home/csi/data

COPY requirements.txt /requirements.txt
RUN python -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir -r /requirements.txt -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple \
  && chown -R csi:csi /venv

COPY --chown=csi:csi . /app/
WORKDIR /app

# Set PYTHONUNBUFFERED to 1 to force the stdout
# and stderr streams to be unbuffered
ENV PATH=/venv/bin:$PATH \
    PYTHONUNBUFFERED=1

USER csi

RUN pip install .

# Running uvicorn is for testing
# For production, run using Gunicorn using the uvicorn worker class
# Use one or two workers per-CPU core
# For example:
# gunicorn -w 4 -k uvicorn.workers.UvicornWorker --log-level warning galaxy_bot.app:app
CMD ["uvicorn", "--host", "0.0.0.0", "--port", "8000", "galaxy_bot.app:app"]
